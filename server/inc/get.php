<?php

function listServers($conn){
    $stmt = $conn->prepare("SELECT * FROM `servers` WHERE `active` = 1");
    $stmt->execute();
    $results = $stmt->fetchAll();

    foreach($results as $index=>$server){

        $stmt = $conn->prepare("SELECT COUNT(`alert_id`) as 'alerts', (SELECT `checkin` FROM `server_checkin` WHERE `server_id` = ? ORDER BY `checkin` DESC LIMIT 1) as 'lastCheckin' FROM `active_alerts` WHERE `server_id` = ? AND `end` IS NULL;");
        $stmt->execute([
            $server['server_id'],
            $server['server_id']
        ]);
        $alerts = $stmt->fetchAll();
        if( !$alerts[0]['alerts'] ? $alertCount = 0 : $alertCount = $alerts[0]['alerts'] );

        echo '<div class="container' . ($index == 0 ? '' : ' border-top' ) . ' p-1">';
        echo '<span class="h3">' . $server['server_friendly'] . '</span><br/>';
        echo '<span class="text-secondary fw-light fst-italic fs-6">Last Checkin: ' . $alerts[0]['lastCheckin'] . '</span><br/>';
        echo '<span>' . ( $alertCount == 0 ? 'No Active Alert' : $alertCount . ' Alert' ) . ( $alertCount != 1 ? 's' : '' ) . '</span>';
        echo '</div>';
    }
}