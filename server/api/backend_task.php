<?php
require_once('connection.php');
require_once('alerts.php');
date_default_timezone_set('America/New_York');

/**
 * Variables
 */
$checkinRange = 15;

function checkServerCheckin($conn,$serverId,$checkinRange){
    $stmt = $conn->prepare("SELECT COUNT(`checkin_id`) as 'checkin', (SELECT `alert_id` FROM `active_alerts` WHERE `server_id` = ? AND `end` IS NULL AND `code` = 0 and `device` = 0) as 'alerts' FROM `server_checkin` WHERE `checkin` > now() - INTERVAL ? MINUTE AND `server_checkin`.`server_id` = ?");
    $stmt->execute([
        $serverId,
        $checkinRange,
        $serverId
    ]);
    $results = $stmt->fetchAll();
    print_r($results);
    echo '<br/>';

    if( $results[0]['checkin'] == 0 && !$results[0]['alerts'] ){
        //Not Checked In
        echo 'Not Checked In <br/>';
        startAlert($conn,$serverId,0,0,0);
    }
}

$stmt = $conn->prepare("SELECT `server_id` FROM `servers` WHERE `active` = 1");
$stmt->execute();
$results = $stmt->fetchAll();
foreach($results as $server){
    checkServerCheckin($conn, $server['server_id'], $checkinRange);
}
