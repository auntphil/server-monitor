<?php

/**
 * INSERT
 */

/**
 * Function
 * Insert a controller
 */
function insert_controller($conn, $serverId, $controller){
    $stmt = $conn->prepare("INSERT INTO `controllers` (`server_id`, `id`, `desc`,`name`) VALUES (?,?,?,?)");
    $stmt->execute([$serverId,$controller['Id'],$controller['Description'],$controller['Name'] ]);
    $controllerId = $conn->lastInsertId();
    return $controllerId;
}

/**
 * Function
 * Insert controller health
 */
function insert_controllerHealth($conn, $controllerId, $controller){
    $stmt = $conn->prepare("INSERT INTO `controllers_health` (`controller_id`, `state`, `health`,`healthrollup`,`timestamp`) VALUES (?,?,?,?,?)");
    $stmt->execute([ $controllerId,$controller['Status']['State'],helperStatus($controller['Status']['Health']),helperStatus($controller['Status']['HealthRollup']),date("Y-m-d H:i:s") ]);
    return;
}

/**
 * Function 
 * Insert Drive
 */
function insert_drive($conn,$serverId,$drive){
    $stmt = $conn->prepare("INSERT INTO `drives` (`server_id`, `id`, `desc`, `capacity`, `hotspare`, `make`, `model`, `connector`, `serial`, `part`, `type`, `rev`, `encAbility`, `encStatus`,`volume`) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
    $volume = explode("/",$drive['Links']['Volumes'][0]['@odata.id']);
    $stmt->execute([ 
        $serverId,
        $drive['Id'],
        $drive['Description'],
        $drive['CapacityBytes'],
        $drive['HotspareType'],
        $drive['Manufacturer'],
        $drive['Model'],
        $drive['Protocol'],
        $drive['SerialNumber'],
        $drive['PartNumber'],
        $drive['MediaType'],
        $drive['Revision'],
        $drive['EncryptionAbility'],
        $drive['EncryptionStatus'],
        end($volume)
    ]);
    $driveId = $conn->lastInsertId();
    return $driveId;
}

function insert_driveHealth($conn,$driveId,$drive){
    $stmt = $conn->prepare("INSERT INTO `drives_health` (`drive_id`, `state`, `health`, `healthrollup`, `timestamp`) VALUES (?,?,?,?,?)");
    $stmt->execute([
        $driveId,
        $drive['Status']['State'],
        helperStatus($drive['Status']['Health']),
        helperStatus($drive['Status']['HealthRollup']),
        date("Y-m-d H:i:s")
    ]);
}

function insert_volume($conn,$serverId,$volume){
    $stmt = $conn->prepare("INSERT INTO `volumes` (`server_id`, `id`, `desc`, `size`, `encrypted`, `name`, `type`) VALUES (?,?,?,?,?,?,?)");
    $stmt->execute([
        $serverId,
        $volume['Id'],
        $volume['Description'],
        $volume['CapacityBytes'],
        helperBoolean($volume['Encrypted']),
        $volume['Name'],
        $volume['VolumeType'],
    ]);
}

function insert_volumeHealth($conn,$volumeId,$volume){
    $stmt = $conn->prepare("INSERT INTO `volumes_health` (`volume_id`, `state`, `health`, `healthrollup`, `timestamp`) VALUES (?,?,?,?,?)");
    $stmt->execute([
        $volumeId,
        $volume['Status']['State'],
        helperStatus($volume['Status']['Health']),
        helperStatus($volume['Status']['HealthRollup']),
        date("Y-m-d H:i:s")
    ]);
}

function insert_checkin($conn,$serverId){
    $stmt = $conn->prepare("INSERT INTO `server_checkin` (`server_id`, `checkin`) VALUES (?,?)");
    $stmt->execute([
        $serverId,
        date("Y-m-d H:i:s")
    ]);
    
    $stmt = $conn->prepare("UPDATE `active_alerts` SET end = ? WHERE `server_id` = ? AND end IS NULL");
    $stmt->execute([
        date("Y-m-d H:i:s"),
        $serverId
    ]);
}