<?php

/**
 * HELPERS
 */

/**
 * Convert Status to INT
 */
function helperStatus($status){
    switch($status){
        case 'OK':
            return 0; //OK
            break;
        case 'Warning':
            return 1; //Warning
            break;
        case 'Critical':
            return 2; //Critical
            break;
        default:
            return 3; //No Data
            break;
    }
}

/**
 * Convert State
 */
function helperState($state){
    switch($state){
        case 'Enabled':
            return 0; //Enabled
            break;
        case 'Disabled':
            return 1; //Disabled
            break;
        case 'Absetn':
            return 2; //Absetn
            break;
        case 'Deferring':
            return 3; //Deferring
            break;
        case 'InTest':
            return 4; //InTest
            break;
        case 'Quiesced':
            return 5; //Quiesced
            break;
        case 'StandbyOffline':
            return 6; //StandbyOffline
            break;
        case 'StandbySpare':
            return 7; //StandbySpare
            break;
        case 'UnavailableOffline':
            return 8; //UnavailableOffline
            break;
        case 'Updating':
            return 9; //Updating
            break;
        default:
            return 10; //No Data
            break;
    }
}

function helperBoolean($bool){
    if($bool){
        return 1;
    }else{
        return 0;
    }
}