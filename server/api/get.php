<?php

/**
 * GET
 */

 /**
 * Function
 * Check if server exists. Unauthorized if no server found
 * Returns server_id
 */
function get_serverID($conn,$server_id,$group_id) {
    $stmt = $conn->prepare("SELECT `servers`.`server_id` FROM `servers` INNER JOIN `clients` on `servers`.`client_id` = `clients`.`client_id` WHERE `servers`.`server_guid` = ? AND `clients`.`client_guid` = ?");
    $stmt->execute([$server_id,$group_id]);
    $results = $stmt->fetchAll();
    if( count($results) !== 1){
        header("HTTP/1.1 401 Unauthorized");
        die();
    }
    return $results[0]['server_id'];
}

/**
 * Function
 * Returns ID of controller if found
 */
function get_controllerID($conn,$server_id,$controller) {
    $stmt = $conn->prepare("SELECT * FROM `controllers` WHERE `controllers`.`server_id` = ? AND `controllers`.`id` = ?");
    $stmt->execute([$server_id,$controller['Id']]);
    $results = $stmt->fetchAll();
    
    if ( count($results) === 1 ){
        return $results[0]['controller_id'];
    } else {
        return false;
    }
}

function get_driveID($conn,$server_id,$drive) {
    $stmt = $conn->prepare("SELECT * FROM `drives` WHERE `server_id` = ? AND id = ?");
    $stmt->execute([
        $server_id,
        $drive['Id']
    ]);
    $results = $stmt->fetchAll();
        
    if( count($results) === 1 ){
        return $results[0]['drive_id'];
    }else{
        false;
    }
}

function get_volumeID($conn,$serverId,$volume) {
    $stmt = $conn->prepare("SELECT * FROM `volumes` WHERE `server_id` = ? AND `id` = ?");
    $stmt->execute([
        $serverId,
        $volume['Id']
    ]);
    $results = $stmt->fetchAll();

    if( count($results) === 1){
        return $results[0]['volume_id'];
    }else{
        false;
    }
}