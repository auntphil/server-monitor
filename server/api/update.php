<?php

/**
 * UPDATE
 */

 /**
  * Function
  * Update Controller Table
  */
function update_controller($conn,$controlerId,$controller){
    $stmt = $conn->prepare("UPDATE `controllers` SET `desc` = ?, `name` = ? WHERE `controller_id` = ?");
    $stmt->execute([ $controller['Description'],$controller['Name'],$controlerId ]);
}

/**
 * Function
 * Update Drive Table
 */
function update_drive($conn,$driveId,$drive){
  $stmt = $conn->prepare("UPDATE `drives` SET `desc` = ?, `hotspare` = ?, `encAbility` = ?, `encStatus` = ?, `volume` = ? WHERE drive_id = ?");
  $volume = explode("/",$drive['Links']['Volumes'][0]['@odata.id']);
  $stmt->execute([
    $drive['Description'],
    $drive['HotspareType'],
    $drive['EncryptionAbility'],
    $drive['EncryptionStatus'],
    end($volume),
    $driveId
  ]);
}

function update_volume($conn,$volumeId,$volume) {
  $stmt = $conn->prepare("UPDATE `volumes` SET `desc` = ?, `encrypted` = ?, `name` = ?, `type` = ? WHERE volume_id = ? ");
  $stmt->execute([
    $volume['Description'],
    $volume['Encrypted'],
    $volume['Name'],
    $volume['VolumeType'],
    $volumeId
  ]);
}