<?php

function checkAlert( $conn, $sid, $id, $data, $device, $deviceId ){
    //Getting Alerts for the device on the server
    $stmt = $conn->prepare("SELECT `alert_id` FROM `active_alerts` WHERE `server_id` = ? AND `end` IS NULL AND `device` = ? AND `device_id` = ?");
    $stmt->execute([
        $sid,
        $device,
        $deviceId
    ]);
    $alerts = $stmt->fetchAll();

    if( helperStatus($data['Status']['Health']) != 0 || helperStatus($data['Status']['HealthRollup']) != 0 ){
        if( count($alerts) <= 0 ){
            $code = ( helperStatus($data['Status']['Health']) > helperStatus($data['Status']['HealthRollup']) ? helperStatus($data['Status']['Health']) : helperStatus($data['Status']['HealthRollup']) );
            startAlert($conn, $sid, $code, $device, $deviceId);
        }
    }else{
        endAlert($conn, $alerts[0]['alert_id']);
    }
}

function startAlert($conn, $serverId, $code, $device, $deviceId){
    echo 'start alert<br/>';
    $stmt = $conn->prepare("INSERT INTO `active_alerts` (`server_id`, `start`, `device`, `device_id`, `code`) VALUES (?,?,?,?,?)");
    $stmt->execute([
        $serverId,
        date("Y-m-d H:i:s"),
        $device,
        $deviceId,
        $code
    ]);
}

function endAlert($conn, $alertId){
    echo 'end alert<br/>';
    $stmt = $conn->prepare("UPDATE `active_alerts` SET `end` = ? WHERE `alert_id` = ?");
    $stmt->execute([
        date("Y-m-d H:i:s"),
        $alertId
    ]);
}