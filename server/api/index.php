<?php

require_once('connection.php');
require_once('get.php');
require_once('insert.php');
require_once('update.php');
require_once('helper.php');
require_once('alerts.php');

date_default_timezone_set('America/New_York');

/**
 * Reading Data from Request
 */
$data = json_decode(file_get_contents("php://input"), true);

/**
 * Function
 * Checks if Server and Group ID are set
 */
if( !isset($data['server_id']) ||
    !isset($data['group_id']) ||
    $data['server_id'] == '' ||
    $data['group_id'] == '' ||
    is_null($data['server_id']) ||
    is_null($data['group_id'])
    ){
        header("HTTP/1.1 401 Unauthorized");
        die();
    }

$serverId = get_serverID($conn,$data['server_id'],$data['group_id']);

foreach( $data['Storage']['Members'] as $controller ){
    $controllerId = get_controllerID($conn,$serverId,$controller);
    if( !$controllerId ){
        $controllerId = insert_controller($conn,$serverId,$controller);
    }else{
        update_controller($conn,$controllerId,$controller);
    }
    insert_controllerHealth($conn,$controllerId,$controller);

    checkAlert($conn, $serverId, $controllerId, $controller, 0, $controllerId );

    #Insert Drives
    foreach( $controller['Drives'] as $drive ){
        $driveId = get_driveID($conn, $serverId, $drive);
        if( !$driveId ){
            $driveId = insert_drive($conn, $serverId, $drive);
        }else{
            update_drive($conn,$driveId,$drive);
        }
        insert_driveHealth($conn,$driveId,$drive);
    }

    #Insert Volumes
    foreach( $controller['Volumes']['Members'] as $volume ){
        $volumeId = get_volumeID($conn, $serverId, $volume );
        if( !$volumeId ){
            $volumeId = insert_volume($conn, $serverId, $volume);
        }else{
            update_volume($conn,$volumeId,$volume);
        }
        insert_volumeHealth($conn,$volumeId,$volume);
    }
}

insert_checkin($conn,$serverId);

