-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 16, 2021 at 04:03 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lighthouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_alerts`
--

CREATE TABLE `active_alerts` (
  `alert_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `device` int(11) NOT NULL,
  `device_id` int(11) NOT NULL,
  `code` int(11) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `clients`
--

CREATE TABLE `clients` (
  `client_id` int(16) NOT NULL,
  `client_guid` text NOT NULL,
  `client_name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `controllers`
--

CREATE TABLE `controllers` (
  `controller_id` int(16) NOT NULL,
  `server_id` text NOT NULL,
  `id` text NOT NULL,
  `desc` text NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `controllers_health`
--

CREATE TABLE `controllers_health` (
  `controller_health_id` int(11) NOT NULL,
  `controller_id` int(11) NOT NULL,
  `state` text DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `healthrollup` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `drives`
--

CREATE TABLE `drives` (
  `drive_id` int(16) NOT NULL,
  `server_id` int(16) NOT NULL,
  `id` text NOT NULL,
  `desc` text DEFAULT NULL,
  `capacity` bigint(20) DEFAULT NULL,
  `hotspare` text DEFAULT NULL,
  `make` text DEFAULT NULL,
  `model` text DEFAULT NULL,
  `connector` text DEFAULT NULL,
  `serial` text DEFAULT NULL,
  `part` text DEFAULT NULL,
  `type` text DEFAULT NULL,
  `rev` text DEFAULT NULL,
  `encAbility` int(11) DEFAULT NULL,
  `encStatus` int(11) DEFAULT NULL,
  `volume` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `drives_health`
--

CREATE TABLE `drives_health` (
  `drive_health_id` int(11) NOT NULL,
  `drive_id` int(11) NOT NULL,
  `state` text DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `healthrollup` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `servers`
--

CREATE TABLE `servers` (
  `server_id` int(16) NOT NULL,
  `server_guid` text NOT NULL,
  `server_tag` text DEFAULT NULL,
  `server_friendly` text NOT NULL,
  `server_hostname` text DEFAULT NULL,
  `client_id` int(16) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `server_checkin`
--

CREATE TABLE `server_checkin` (
  `checkin_id` int(11) NOT NULL,
  `server_id` int(11) NOT NULL,
  `checkin` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `volumes`
--

CREATE TABLE `volumes` (
  `volume_id` int(16) NOT NULL,
  `server_id` bigint(20) NOT NULL,
  `id` text DEFAULT NULL,
  `desc` text DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `encrypted` tinyint(1) DEFAULT NULL,
  `name` text DEFAULT NULL,
  `type` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `volumes_health`
--

CREATE TABLE `volumes_health` (
  `volume_health_id` int(11) NOT NULL,
  `volume_id` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `health` int(11) DEFAULT NULL,
  `healthrollup` int(11) DEFAULT NULL,
  `timestamp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `active_alerts`
--
ALTER TABLE `active_alerts`
  ADD PRIMARY KEY (`alert_id`);

--
-- Indexes for table `clients`
--
ALTER TABLE `clients`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `controllers`
--
ALTER TABLE `controllers`
  ADD PRIMARY KEY (`controller_id`);

--
-- Indexes for table `controllers_health`
--
ALTER TABLE `controllers_health`
  ADD PRIMARY KEY (`controller_health_id`);

--
-- Indexes for table `drives`
--
ALTER TABLE `drives`
  ADD PRIMARY KEY (`drive_id`);

--
-- Indexes for table `drives_health`
--
ALTER TABLE `drives_health`
  ADD PRIMARY KEY (`drive_health_id`);

--
-- Indexes for table `servers`
--
ALTER TABLE `servers`
  ADD PRIMARY KEY (`server_id`);

--
-- Indexes for table `server_checkin`
--
ALTER TABLE `server_checkin`
  ADD PRIMARY KEY (`checkin_id`);

--
-- Indexes for table `volumes`
--
ALTER TABLE `volumes`
  ADD PRIMARY KEY (`volume_id`);

--
-- Indexes for table `volumes_health`
--
ALTER TABLE `volumes_health`
  ADD PRIMARY KEY (`volume_health_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `active_alerts`
--
ALTER TABLE `active_alerts`
  MODIFY `alert_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `clients`
--
ALTER TABLE `clients`
  MODIFY `client_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `controllers`
--
ALTER TABLE `controllers`
  MODIFY `controller_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `controllers_health`
--
ALTER TABLE `controllers_health`
  MODIFY `controller_health_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drives`
--
ALTER TABLE `drives`
  MODIFY `drive_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `drives_health`
--
ALTER TABLE `drives_health`
  MODIFY `drive_health_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `servers`
--
ALTER TABLE `servers`
  MODIFY `server_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `server_checkin`
--
ALTER TABLE `server_checkin`
  MODIFY `checkin_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `volumes`
--
ALTER TABLE `volumes`
  MODIFY `volume_id` int(16) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `volumes_health`
--
ALTER TABLE `volumes_health`
  MODIFY `volume_health_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
