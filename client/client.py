#!/usr/bin/env python
import requests, sys, json
from datetime import datetime
requests.packages.urllib3.disable_warnings()

with open('test-data/test.json') as json_file:
    data = json.load(json_file)

print (datetime.now().strftime("%H:%M:%S"))

####
# Server Variables
server_id = "a65s4f65as4f6asf464f879e"
group_id = "a58sd4f96a84f9a7f"
server_url = "https://localhost/api/"

# Client Variables
idrac_ip = "10.114.97.90"
idrac_username = "tsimonitor"
idrac_password = "zYEMlx4wqW0nx"
###

def checkRequest(response):
    if response.status_code == 200 or response.status_code == 202:
        return response
    else:
        print("- FAIL, GET command failed, detailed error information: %s" % response)
        sys.exit()
    
def sendRequest(data):
    response = requests.put(server_url, json=data, verify=False)
    print (response.text)

#sendRequest(data)
#sys.exit()


###
#Classes
server = {
    'server_id' : server_id,
    'group_id' : group_id,
}

###
#Get Overall Server Stats
req = checkRequest(requests.get('https://%s/redfish/v1/Systems/System.Embedded.1/' % (idrac_ip),verify=False,auth=(idrac_username,idrac_password)))
overall_data = req.json()
server = req.json()

#Get BIOS Information
server["Bios"] = checkRequest(requests.get('https://%s%s' % (idrac_ip,overall_data['Bios']['@odata.id']),verify=False,auth=(idrac_username,idrac_password))).json()


##Getting PSU Information
server['Links']['PoweredBy'] = []
for psu in overall_data['Links']['PoweredBy']:
    #/redfish/v1/Chassis/System.Embedded.1/Power/PowerSupplies/PoweredBy
    server['Links']['PoweredBy'].append(checkRequest(requests.get('https://%s%s' % (idrac_ip, psu['@odata.id'] ),verify=False,auth=(idrac_username,idrac_password))).json())


##Getting List of Controllers
#/redfish/v1/Systems/System.Embedded.1/Storage
req = checkRequest(requests.get('https://%s%s' % (idrac_ip,overall_data['Storage']['@odata.id']),verify=False,auth=(idrac_username,idrac_password)))
storage_data = req.json()
server['Storage'] = req.json()
server['Storage']['Members'] = []

#Loop Through Controllers
controller_count = 0
for controller in storage_data['Members']:
    ##Getting list of Disks and Volumes
    #/redfish/v1/Systems/System.Embedded.1/Storage/RAID.Integrated.1-1
    req = checkRequest(requests.get('https://%s%s' % (idrac_ip, controller['@odata.id']),verify=False,auth=(idrac_username,idrac_password)))
    
    controller_data = req.json()
    newController = req.json()

    #Loop through Drives
    newController['Drives'] = []
    for d in controller_data['Drives']:
        #Get individual Drive
        #/redfish/v1/Systems/System.Embedded.1/Storage/Drives/Disk.Bay.2:Enclosure.Internal.0-1:RAID.Integrated.1-1
        newController['Drives'].append(checkRequest(requests.get('https://%s%s' % (idrac_ip, d['@odata.id'] ),verify=False,auth=(idrac_username,idrac_password))).json())

    #Get List of Volumes on the Controller
    #/redfish/v1/Systems/System.Embedded.1/Storage/RAID.Integrated.1-1/Volumes
    req = checkRequest(requests.get('https://%s%s' % (idrac_ip, controller_data['Volumes']['@odata.id']),verify=False,auth=(idrac_username,idrac_password)))
    volumes_data = req.json()
    newController['Volumes'] = req.json()
    newController['Volumes']['Members'] = []

    #Get individual Volume
    for vm in volumes_data['Members']:
        #/redfish/v1/Systems/System.Embedded.1/Storage/Volumes/Disk.Virtual.0:RAID.Integrated.1-1
        newController['Volumes']['Members'].append(checkRequest(requests.get('https://%s%s' % (idrac_ip, vm['@odata.id'] ),verify=False,auth=(idrac_username,idrac_password))).json())
    server['Storage']['Members'].append(newController)

#Adding Server and Client Information
server['server_id'] = server_id
server['group_id'] = group_id

with open('test-data/test.json', 'w') as outfile:
    json.dump(server, outfile)
#sendRequest(server)
print (datetime.now().strftime("%H:%M:%S"))